module RoomsHelper

    def get_percentages day_min, day_max, room
        sum_days = 0
        sum_days += room.reservations.count_days(day_min, day_max)
 
        helper.number_to_percentage(Rational(sum_days, day_max) * 100, precision: 0)
    end


    def helper
        @helper ||= Class.new do
            include ActionView::Helpers::NumberHelper
        end.new
    end       
    
end
