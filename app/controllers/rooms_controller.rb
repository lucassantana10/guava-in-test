class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]

  def index
    @rooms = Room.all
    @percentage_per_week = get_percentages(1,7)
    @percentage_per_month = get_percentages(1,30)
  end

  def show
  end

  def new
    @room = Room.new
  end

  def edit
  end

  def create
    @room = Room.new(room_params)

    if @room.save
      redirect_to @room, notice: 'Room was successfully created.'
    else
      render :new
    end
  end

  def update
    if @room.update(room_params)
      redirect_to @room, notice: 'Room was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @room.destroy
    redirect_to rooms_url, notice: 'Room was successfully destroyed.'
  end

  private
    def set_room
      @room = Room.find(params[:id])
    end

    def get_percentages day_min, day_max
      sum_days = 0
      @rooms.each do |room|
        sum_days += room.reservations.count_days(day_min, day_max)
      end    
      helper.number_to_percentage(Rational(sum_days, day_max) * 100, precision: 0)
    end

    def room_params
      params.require(:room).permit(:code, :capacity, :notes)
    end

    def helper
      @helper ||= Class.new do
        include ActionView::Helpers::NumberHelper
      end.new
    end    
end
