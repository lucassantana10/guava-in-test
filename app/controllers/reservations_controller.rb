class ReservationsController < ApplicationController
  def search
    @should_show_results = params[:start_date].present? &&
                           params[:end_date].present? &&
                           params[:number_of_guests].present?
                                                                
    @available_rooms = rooms_to_show(
                       Room.equals_or_more_than(
                            params[:number_of_guests]))

  end

  def new
    @reservation = Reservation.new(reservation_params)
    get_room
  end

  def create
    @reservation = Reservation.new(reservation_params)
    if @reservation.save
      redirect_to @reservation.room,
        notice: "Reservation #{@reservation.code} was successfully created."
    else
      render :new
    end
  end

  def destroy
    @reservation = Reservation.find(params[:id])
    @reservation.destroy
    redirect_to room_path(@reservation.room),
      notice: "Reservation #{@reservation.code} was successfully destroyed."
  end

  private

  def rooms_to_show rooms
    rooms_selected = []
    rooms.each do |room|
      block_room = false
      room.reservations.each do |reservation|  
        if reservation.end_date.to_datetime > params[:start_date].to_datetime
          block_room = true 
        end
      end
      rooms_selected << room if !block_room
    end 
     @should_show_results ? rooms_selected : Room.none 
  end

  def get_room
    @room = Room.find(params[:reservation][:room_id])
  end

  def reservation_params
    params.require(:reservation).permit(:start_date, :end_date, :number_of_guests, 
                                        :guest_name, :room_id)
  end
end
