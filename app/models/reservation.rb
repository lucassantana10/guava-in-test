class Reservation < ApplicationRecord
  belongs_to :room

  validates_presence_of :start_date, :end_date, :guest_name, :number_of_guests
  validates_numericality_of :number_of_guests, greater_than: 0, less_than_or_equal_to: 10
  validate :start_date_is_before_end_date

  def duration
    if start_date.present? && end_date.present? && end_date > start_date
      (end_date - start_date).to_i
    end
  end

  def code
    if id.present? && room&.code.present?
      formatted_id = '%02d' % id
      "#{room.code}-#{formatted_id}"
    end
  end

  def duration_in_week_and_month param_date
    if param_date > start_date
      (end_date - param_date).to_i
    else
      (end_date - start_date).to_i
    end
  end

  def self.count_days(min, max)
    sum_days = 0;
    self.where("start_date >= ? AND end_date <= ?", 
                          (Time.now + min.day), 
                          (Time.now + max.day)).each do |reservation|
                            sum_days += reservation.duration_in_week_and_month((Time.now + min.day).to_date)
    end
    sum_days
  end


  private

  def start_date_is_before_end_date
    if start_date.present? && end_date.present? && start_date >= end_date
      errors.add(:base, :invalid_dates, message: 'The start date should be before the end date')
    end
  end
end
