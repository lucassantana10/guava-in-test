require 'rails_helper'

RSpec.describe 'Reservations', type: :system do
  before do
    driven_by(:selenium_chrome_headless)
  end

  describe 'Show Reservation form' do

    it 'Show form to search a reservation' do
      visit search_reservations_path

      expect(page.find_field('start_date').value).to eq Time.now.to_date.to_s
      expect(page.find_field('end_date').value).to eq (Time.now + 2.day).to_date.to_s
      expect(page.find_field('number_of_guests').value).to eq '1'

      expect(page).to have_selector("input[type=submit][value='Search for Available Rooms']")
    end

    it 'Show empty result when putted period and number_of_guests' do
      visit search_reservations_path

      page.find('#number_of_guests').find(:xpath, 'option[9]').select_option

      expect(page.find_field('start_date').value).to eq Time.now.to_date.to_s
      expect(page.find_field('end_date').value).to eq (Time.now + 2.day).to_date.to_s
      expect(page.find_field('number_of_guests').value).to eq '9'   
      form = find('input[type=submit]').click

      within('table') do
        within('thead') do
          expect(page).to have_content('Code')
          expect(page).to have_content('Capacity')
          expect(page).to have_content('Actions')
        end

        within('tbody tr:first-child') do
          expect(page).to have_content('There are no available rooms for the selected filters.')
        end
      end
    end
  end
end
