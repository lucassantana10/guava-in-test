require 'rails_helper'

RSpec.describe ReservationsController, type: :controller do
    it "search action should be success" do
        get :search, :params => {:start_date => '2020-07-18', :end_date => '2020-07-20', :number_of_guests => '5'}
        assert_response :success
    end 
end