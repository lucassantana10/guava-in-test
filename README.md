# README
Sejam bem-vindos ao respositório, aqui você irá encontrar as melhorias e correções de erros passados, tais como testes automatizados implementados.

## Software necessário

- Ruby 2.7.1
- PostgreSQL 11 ou superior
- [Yarn](https://yarnpkg.com/en/docs/install)
- [ChromeDriver](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)

## Instruções de execução

Para fazer o setup geral do projeto (incluindo a criação de _seeds_):

```
$ bin/setup
```

Para executar os testes:

```
$ bundle exec rspec
```

Para executar o servidor:

```
$ bundle exec rails server
```

# New Features!

  - Ajuste na busca por reservas, agora ao consultar as reservas o sistema exibirá apenas os quartos com a quantidade igual ou superior a escolhida no filtro.
  - Ajuste no formulário de Reservas, ao clicar no quarto escolhido para fazer a reserva, o formulário já trará a quantidade maxima que pode ser escolhida referente ao limite de pessoas para o quarto selecionado.
  - Ajuste de criação de reserva com check-out até meio dia
  - Cálculo de taxas de ocupação mensal e semanal.
  - Correção de teste automatizado.
  - Inserção de novos testes automatizados para a tela de Reservas.
  - Correção de valores na Seed


### Decisões tomadas
####  #1
Foi removida a gem 
```gem 'rspec-rails', '~> 3.9'```
e no lugar foi inserida a gem
```sh 
gem 'rspec-rails', '~> 4.0.0.beta3'
```
Estava tendo problemas ao testar o controllador com a versão do rspec, fazendo algumas pesquisas vi que algumas pessoas também tiveram o mesmo problema e alguns relataram que era questão de compatibilidade com a versão do rails 6. Após fazer a troca, foi testado todos os testes e a gem funcionou perfeitamente tanto nos testes antigos como nos novos testes.

####  #2

![Tela de cadastro da reserva](<https://www.dropbox.com/s/a8k88my1vzi47ns/FireShot%20Capture%20007%20-%20Guava%20Inn%20-%20localhost.png?dl=0>)

Tomei a decisão de travar o limite de pessoas por quarto no formulário de reserva, pois assim evitaria o usuário de errar. Também travei o quarto, pois já que o usuário consegue enxergar a lista de quartos disponíveis na busca, travei ao ponto de evitar que o usuário cometa o erro de selecionar um quarto indisponível.

#### #3
Foi visto um erro na Seed, onde uma reserva estava sendo iniciada antes do término de outra reserva, foi reportado por email e foi solicitado autorização para a alteração

#### #4
Tomei a decisão de publicar este projeto em produção, no heroku. Segue o link: 
##### https://guava-in-desafio.herokuapp.com/

### Considerações Finais
Em primeiro lugar venho agradecer a oportunidade de participar do desafio, realmente eu gostei muito e foi algo que sempre quis fazer em uma seleção, resolver problemas "reais". Pode ter certeza que eu me empenhei ao máximo para poder entregar este projeto e foi um prazer imenso faze-lo.
